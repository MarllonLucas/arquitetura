# ---- Testa addi, addiu, add, sub, mult, div, mfhi, mflo ----- #

.text 
	addi $t0, $zero, 1				# t0 =1
	addi $t1, $zero, 10				# t1 = 10
	addi $t2, $zero, -7				# t2 = -7
	addiu $t3, $zero, 4				# t3 = 4
	
	add $t4, $t0, $t2				# t4 = t0 + t2
	sub $t5, $t3, $t2				# t5 = t3 - t2
	mult $t1, $t2					# HILO = t1 * t2
	mfhi $t6					# t6 = 32 bits mais sigificativos do HILO
	mflo $t7					# t7 = 32 bits menos significativos do HILO
	
	div $t1, $t3					# HI = t1%t3, LO = t1/t3
	mfhi $t8					# t8 = 32 bits mais sigificativos do HILO
	mflo $t9					# t9 = 32 bits menos significativos do HILO
	
	#syscall
	