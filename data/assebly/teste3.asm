# ---- Testa slti, bne, and, or, andi, ori, sll, srl  ----- #

.text 
	
	addiu $t0, $zero, 0xffff				# t0 = 0xffff
	sll $t0, $t0, 16					# t0 = 0xffff0000
	addiu $t0, $t0, 0xffff					# t0 = axffffffff
	srl $t1, $t0, 31  					# t1 = 1
	addi $t1, $t1, -1
	addi $t2, $zero, 1					# t2 = 1
	addi $t3, $zero, 0xff01					# t3 = 0xff01
		
laco:	slti $t4, $t1, 1
	bne $t4, $t2, fim					# se t1 < 1 entre no laco
	
	and $t5, $t0, $t3					# t5 = t0 & t3
	or $t6, $t0, $t3					# t6 = t0 | t3
	
	andi $t7, $t3, 0x0001					# t7 = t3 & 1
	ori $t8, $t3, 0x3000					# t8 = t3 or 0x3000
	
	addi $t1, $t1, 1					# t1 += 1
	
	j laco
fim:	syscall