# ----- Testa lw, lh, lb, sw, sh, sb, lui, sra ------ #

.data
numero: .word 0x1111ccca
mem1: 	.word 0x0
mem2:	.word 0x0
mem3:	.word 0x0
.text
	lw $t0, numero				# t0 = 8 bytes da palavra
	lh $t1, numero				# t1 = 4 bytes da palavra (completa com 1s a esquerda)
	lb $t2, numero				# t2 = 1 byte da palavra (completa com 1s a esquerda)
	
	addiu $t4, $t4, 0xaaaa			#
	sll $t4, $t4, 16			# t4 = 0xaaaabbcc 
	addi $t5, $t5, 0xbbbc			#
	add $t4, $t4, $t5			#
	
	sw $t4, mem1				# mem1 = 8 bytes de t4
	sh $t4, mem2				# mem2 = 4 bytes de t4
	sb $t4, mem3				# mem3 = 1 byte de t4
	
	lui $t6, 0xdddd				# t6 = dddd0000 (dddd << 16)
	
	sra $t7, $t6, 15			# t7 = dddd0000 >> 15 (com extensao de sinal)
