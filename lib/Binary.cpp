#include "include/Binary.h"

Binary::Binary(){}

void Binary::setPath(char * PathBin, int Size){
	Path = PathBin;
	size = Size;
}

void Binary::Init(){
	// Aloca uma memoria para o recebimento dos codigos binarios
	_Binary = (int *) malloc(sizeof(int) * size);

	// Abre o arquivo binario
	File = fopen(Path, "rb");

	// Le os valores armazenados no arquivo em binario
	// Armazena o numero de dados armazenados
	numberOfData = fread(_Binary, sizeof(int), size , File); 

	fclose(File);

}

// Retona o numero de dados encontrados
int Binary::getNumberOfData(){
	return numberOfData;
}

// Desaloca a memoria alocada
void Binary::close(){
	free(_Binary);
}