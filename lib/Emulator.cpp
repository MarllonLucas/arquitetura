#include "include/Emulator.h"


Emulator::Emulator(char * PathText, char * PathData){

	// Faz a leitura da sessao de texto
	_BinaryMipsText.setPath(PathText, 0x0FFF);
	_BinaryMipsText.Init();
	
	// Faz a leitura da sessao de dados
	_BinaryMipsData.setPath(PathData, 0x0FFF);
	_BinaryMipsData.Init();

}

void Emulator::CopyTextToMemory(){
	int Address = 0x0;
	int NumberInstructions = _BinaryMipsText.getNumberOfData();

	for (int i = 0; i < NumberInstructions; ++i, Address += 8){
		_RAM.update(Address, _BinaryMipsText._Binary[i]);
	}

}

void Emulator::CopyDataToMemory(){
	int Address = 0x2000*2;
	int NumberInstructions = _BinaryMipsData.getNumberOfData();

	for (int i = 0; i < NumberInstructions; ++i, Address += 8){
		_RAM.update(Address, _BinaryMipsData._Binary[i]);
	}
}

void Emulator::run(){

	CopyDataToMemory();
	CopyTextToMemory();

	// Variavel que armazena o valor do PC
	int i = 1;
	int FL = 0;
	int Opcode;
	int Instruction = 1;

	int rs = 0, rd = 0, rt = 0, shamt, immediate, flag;

	// Roda os camandos MIPS
	while(1){
		// Pega o valor do pc
		i = _Register.read($pc);
		FL = _Register.read($FL);

		// Pega a instrução que sera executada
		Instruction = _RAM.read(i);

		// Caso a instrução esteja zerada sai da emulação
		if (Instruction == 0x0 || FL)
			break;

		// Pega o opcode da instrução
		Opcode = _Tools.OpcodeDecode(Instruction);

		switch(Opcode){
			case 0x0:
				int funct;
				funct = _Tools.RegisterFunct(Instruction);

				switch(funct){
					case ADDFNC:

						rd = _Tools.RegisterRD(Instruction);
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.add(rd, rs, rt);

						_MIPS.updatePC();

						break;

					case ADDUFNC:

						rd = _Tools.RegisterRD(Instruction);
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.addu(rd, rs, rt);

						_MIPS.updatePC();

						break;
					
					case SUBFNC:
						rd = _Tools.RegisterRD(Instruction);
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.sub(rd, rs, rt);

						_MIPS.updatePC();

						break;

					case DIVFNC:
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.div(rs, rt);

						_MIPS.updatePC();

						break;

					case MULTFNC:
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.mult(rs, rt);

						_MIPS.updatePC();

						break;

					case ANDFNC:

						rd = _Tools.RegisterRD(Instruction);
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						


						_MIPS.And(rd, rs, rt);

						_MIPS.updatePC();

						break;

					case ORFNC:

						rd = _Tools.RegisterRD(Instruction);
						rs = _Tools.RegisterRS(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						
						_MIPS.Or(rd, rs, rt);



						_MIPS.updatePC();

						break;

					case SLLFNC:

						rd = _Tools.RegisterRD(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						shamt = _Tools.RegisterShamt(Instruction);

						_MIPS.sll(rd, rt, shamt);

						_MIPS.updatePC();

						break;
				
					case SRLFNC:

						rd = _Tools.RegisterRD(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						shamt = _Tools.RegisterShamt(Instruction);


						_MIPS.srl(rd, rt, shamt);

						_MIPS.updatePC();

						break;

					case SRAFNC:
						rd = _Tools.RegisterRD(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						shamt = _Tools.RegisterShamt(Instruction);

						_MIPS.sra(rd, rt, shamt);

						_MIPS.updatePC();

						break;

					case SLTFNC:
						rd = _Tools.RegisterRD(Instruction);
						rt = _Tools.RegisterRT(Instruction);
						rs = _Tools.RegisterRS(Instruction);

						_MIPS.slt(rd, rs, rt);
						_MIPS.updatePC();

						break;

					case MFHIFNC:
						rd = _Tools.RegisterRD(Instruction);

						_MIPS.mfhi(rd);
						_MIPS.updatePC();

						break;

					case MFLOFNC:
						rd = _Tools.RegisterRD(Instruction);

						_MIPS.mflo(rd);
						_MIPS.updatePC();

						break;

					case JRFNC:

						rs = _Tools.RegisterRS(Instruction);
						_MIPS.jr(rs);						
						break;

					case SYSCALLFNC:

						rd = _Register.read($v0);

						_MIPS.syscall(rd);

						_MIPS.updatePC();

						break;
					
					default:
						_MIPS.updatePC();
						break;
				}

				break;

			case ADDI:

				rs = _Tools.RegisterRS(Instruction);
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);


				_MIPS.addi(rt, rs, immediate);

				_MIPS.updatePC();

				break;

			case ADDIU:
				rs = _Tools.RegisterRS(Instruction);
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.addiu(rt, rs, immediate);

				_MIPS.updatePC();

				break;

			case ANDI:
				rt = _Tools.RegisterRT(Instruction);
				rs = _Tools.RegisterRS(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.Andi(rt, rs, immediate);

				_MIPS.updatePC();

				break;

			case ORI:

				rt = _Tools.RegisterRT(Instruction);
				rs = _Tools.RegisterRS(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.Ori(rt, rs, immediate);

				_MIPS.updatePC();

				break;

			case SLTI:

				rt = _Tools.RegisterRT(Instruction);
				rs = _Tools.RegisterRS(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.slti(rt, rs, immediate);

				_MIPS.updatePC();

				break;

			case LUI:

				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);



				_MIPS.lui(rt, immediate);

				_MIPS.updatePC();

				break;

			case LW:
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.lw(rt, immediate*2);

				_MIPS.updatePC();

				break;

			case LH:
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.lh(rt, immediate*2);

				_MIPS.updatePC();

				break;

			case LB:
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.lb(rt, immediate*2);

				_MIPS.updatePC();

				break;

			case SW:

				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.sw(rt, immediate*2);
				_MIPS.updatePC();

				break;

			case SH:
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.sh(rt, immediate*2);

				_MIPS.updatePC();

				break;

			case SB:
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				_MIPS.sb(rt, immediate*2);

				_MIPS.updatePC();

				break;

			case BEQ:
				rs = _Tools.RegisterRS(Instruction);
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				flag = _MIPS.beq(rt, rs);

				if (flag){
					flag = immediate & 0XF000;
					flag >>= 12;
					if(flag == 0xF)
						immediate = 0Xffff0000 | immediate;

					_MIPS.updatePC(i + (immediate)*8 + 8);
				}else{
					_MIPS.updatePC();
				}

				break;

			case BNE:
				rs = _Tools.RegisterRS(Instruction);
				rt = _Tools.RegisterRT(Instruction);
				immediate = _Tools.RegisterImmediate(Instruction);

				flag = _MIPS.bne(rt, rs);
				
				if (flag){
					flag = immediate & 0XF000;
					flag >>= 12;
					if(flag == 0xF)
						immediate = 0Xffff0000 | immediate;

					_MIPS.updatePC(i + (immediate)*8 + 8);
				}else{

					_MIPS.updatePC();
				}

				break;

			case J:

				immediate = _Tools.RegisterImmediateTypeJ(Instruction);
				_MIPS.j(immediate);
				
				break;
			
			case JAL:

				immediate = _Tools.RegisterImmediateTypeJ(Instruction);

				_MIPS.jal(immediate);

				break;

			default:
				_MIPS.updatePC();
				break;
		}	
	}


}

void Emulator::ShowResults(){
	_Register.ShowRegisters();
	_RAM.ShowMemory();
}

void Emulator::close(){
	_BinaryMipsText.close();
	_BinaryMipsData.close();
	_RAM.Clean();
	_Register.FreeRegister();
}