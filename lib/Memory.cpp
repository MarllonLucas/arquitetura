#include "include/Memory.h"
#include "include/Defines.h"
#include <stdio.h>
#include <stdlib.h>

Memory::Memory(){
	// Aloca a variavel que armazena os dados da memoria
	_RAM = (char *) malloc(sizeof(char) * SIZE_MEMORY + 1);
	// Inicia os valores
	Init();
}

void Memory::ShowMemory(){

	// Nesse laco eu percorro todo o vetor de 32 em 32 bits
	for (int i = 0; i < SIZE_MEMORY; i += 32){
		// Para cada linha eu imprimo o endereco da memoria
		printf("MEM[0x%08x]", (i/2));

		// Nesse laco eu percorro da posicao i até i+32
		// fazendo com que eu consiga imprimir a linha inteira
		for (int j = i; j < i + 32	; ++j){
			// Caso a posicao atual for multiplo de 8 eu imprimo o espaco
			if(j%8==0) printf("\t0x");
			// Imprimo o valor hexadecimal da memoria
			printf("%c", _RAM[j]);
		}
		printf("\n");
	}
}


void Memory::Init(){
	// Para cada posição da memoria coloco um valor zero
	for (int i = 0; i < SIZE_MEMORY; i++){
		_RAM[i] = '0';
	}

	// Na ultima posição da memoria eu coloco um \0
	_RAM[SIZE_MEMORY] = 0;
	
}

void Memory::Clean(){
	free(_RAM);
}

// Função para atualização da palavra na memoria
void Memory::update(int Adreess, int Value){
	// Armazena o valor do inteiro em um char
	char * Update = _Tools.HexaEncode(Value);

	int i,j;

	for (i = Adreess, j = 0; i < Adreess+8; i++, j++)
		_RAM[i] = Update[j];

}

// Função para leitura da palavra na memoria
int Memory::read(int Adreess){
	// Cria um vetor para armazerar o valor encontrado na memoria
	char Return[9];

	int i, j;

	for (i = Adreess, j = 0; j < 8; i++, j++)
		Return[j] = _RAM[i];
	
	// Na ultima posicao da string coloca o \0
	Return[j] = 0;

	return _Tools.HexaDecode(Return);
}
