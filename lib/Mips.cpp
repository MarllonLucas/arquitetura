#include "include/Mips.h"


Mips::Mips(Register * REG, Memory * MEM){
	_Register = REG;
	_RAM = MEM;
}

void Mips::updatePC(int32 PC){
	/*Caso o valor de pc nao for auterado atualiso para a proxima instrucao*/
	int32 i = _Register->read($pc);
	if (PC == 0)
		i += 8;
	else
		i = PC;

	_Register->update($pc, i);
}

void Mips::add(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	int32 rs = _Register->read(KeyRegTwo);
	int32 rt = _Register->read(KeyRegThree);

	int32 add = rs + rt;
	_Register->update(KeyRegOne, add);
}

void Mips::addu(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	unsigned int32 rs = _Register->read(KeyRegTwo);
	unsigned int32 rt = _Register->read(KeyRegThree);

	unsigned int32 add = rs + rt;
	_Register->update(KeyRegOne, add);
}


void Mips::addi(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate){
	int32 rs = _Register->read(KeyRegTwo);

	int Flag = Immediate & 0XF000;
	Flag >>= 12;
	if(Flag == 0xF)
		Immediate = 0Xffff0000 | Immediate;

	int32 addi = rs + Immediate;
	_Register->update(KeyRegOne, addi);
}

void Mips::addiu(int32 KeyRegOne, int32 KeyRegTwo, unsigned int32 Immediate){
	int32 rs = _Register->read(KeyRegTwo);

	int32 addiu = rs + Immediate;
	_Register->update(KeyRegOne, addiu);
}

void Mips::sub(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	int32 rs = _Register->read(KeyRegTwo);
	int32 rt = _Register->read(KeyRegThree);

	int32 sub = rs - rt;
	_Register->update(KeyRegOne, sub);
}

void Mips::mult(int32 KeyRegOne, int32 KeyRegTwo){
	int32 rs = _Register->read(KeyRegOne);
	int32 rt = _Register->read(KeyRegTwo);

	int32 HILO = rs * rt;
	// Registrador do HILO
	_Register->update(LO, HILO);
}

void Mips::div(int32 KeyRegOne, int32 KeyRegTwo){
	int32 rs = _Register->read(KeyRegOne);
	int32 rt = _Register->read(KeyRegTwo);


	int32 divHI = rs % rt;
	int32 divLO = rs / rt;

	// Registrador do HI
	_Register->update(HI, divHI);
	// Registrador do LO
	_Register->update(LO, divLO);
}

void Mips::And(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	int32 rs = _Register->read(KeyRegTwo);
	int32 rt = _Register->read(KeyRegThree);

	int32 Value = rs & rt;
	_Register->update(KeyRegOne, Value);
}
// 'E' logico entre registrador e imediato
void Mips::Andi(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate){
	int32 rs = _Register->read(KeyRegTwo);

	int Flag = Immediate & 0XF000;
	Flag >>= 12;
	if(Flag == 0xF)
		Immediate = 0Xffff0000 | Immediate;

	int32 Value = rs & Immediate;
	_Register->update(KeyRegOne, Value);
}
// 'Ou' logico entre registradores
void Mips::Or(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	int32 rs = _Register->read(KeyRegTwo);
	int32 rt = _Register->read(KeyRegThree);

	int32 Value = rs | rt;
	_Register->update(KeyRegOne, Value);
}
// 'Ou' logico entre registrador e imediato
void Mips::Ori(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate){
	int32 rs = _Register->read(KeyRegTwo);

	int Flag = Immediate & 0XF000;
	Flag >>= 12;
	if(Flag == 0xF)
		Immediate = 0Xffff0000 | Immediate;

	int32 Value = rs | Immediate;

	_Register->update(KeyRegOne, Value);
}

/* Funções de manipulação de memoria */
void Mips::sw(int32 KeyRegOne, int32 Address){
	int32 Value = _Register->read(KeyRegOne);
	_RAM->update(Address, Value);
}

void Mips::sh(int32 KeyRegOne, int32 Address){
	int32 Value = _Register->read(KeyRegOne);
	// Pega metade do valor
	Value = 0x0000ffff & Value;
	_RAM->update(Address, Value);
}

void Mips::sb(int32 KeyRegOne, int32 Address){
	int32 Value = _Register->read(KeyRegOne);
	// Pega metade do valor
	Value = 0x000000ff & Value;
	_RAM->update(Address, Value);
}

void Mips::lw(int32 KeyRegOne, int32 Address){
	int32 Value = _RAM->read(Address);
	_Register->update(KeyRegOne, Value);
}

void Mips::lh(int32 KeyRegOne, int32 Address){
	int32 Value = _RAM->read(Address);
	// Pega metade do valor
	Value = 0x0000ffff & Value;
	// Preencha os valores que sairam com F's
	Value = 0Xffff0000 | Value;
	_Register->update(KeyRegOne, Value);
}

void Mips::lb(int32 KeyRegOne, int32 Address){
	int32 Value = _RAM->read(Address);
	// Pega metade do valor
	Value = 0x000000ff & Value;
	// Preencha os valores que sairam com F's
	Value = 0Xffffff00 | Value;
	_Register->update(KeyRegOne, Value);
}

void Mips::lui(int32 KeyRegOne, int32 C){
	C <<= 16;
	_Register->update(KeyRegOne, C);
}

void Mips::mfhi(int32 KeyRegOne){
	int32 hi = _Register->read(HI);	
	_Register->update(KeyRegOne, hi);
}

void Mips::mflo(int32 KeyRegOne){
	int32 lo = _Register->read(LO);	
	_Register->update(KeyRegOne, lo);
}

void Mips::slt(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree){
	int32 rs = _Register->read(KeyRegTwo);
	int32 rt = _Register->read(KeyRegThree);

	int32 Value = 0;

	if (rs < rt)
		Value = 1;

	_Register->update(KeyRegOne, Value);
}

void Mips::slti(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate){
	int32 rs = _Register->read(KeyRegTwo);

	int32 Value = 0;

	if (rs < Immediate)
		Value = 1;

	_Register->update(KeyRegOne, Value);
}

// Deslocamento de bits para a esquerda
void Mips::sll(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt){
	int32 rt = _Register->read(KeyRegTwo);

	rt <<= Shamt; 

	_Register->update(KeyRegOne, rt);

}
// Deslocamento de bits para a direita
void Mips::srl(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt){
	int32 rt = _Register->read(KeyRegTwo);
	int32 calc = 0xFFFFFFFF >> Shamt;
	rt = rt & calc;

	_Register->update(KeyRegOne, rt);
}
// Deslocamento de bits para a direita com extensao de sinal
void Mips::sra(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt){
	int32 rt = _Register->read(KeyRegTwo);

	rt = ((signed int) rt) >> Shamt; 

	_Register->update(KeyRegOne, rt);
}

int32 Mips::beq(int32 KeyRegOne, int32 KeyRegTwo){
	int32 rs = _Register->read(KeyRegOne);
	int32 rt = _Register->read(KeyRegTwo);
	return (rs == rt);

}

int32 Mips::bne(int32 KeyRegOne, int32 KeyRegTwo){
	int32 rs = _Register->read(KeyRegOne);
	int32 rt = _Register->read(KeyRegTwo);
	return (rs != rt);
}

void Mips::j(int32 C){
	
	int Address;
	Address = _Register->read($pc) & 0xF0000000;
	C <<= 2;
	Address = Address | C;

	updatePC(Address*2);

}

void Mips::jr(int32 KeyRegOne){
	int32 rs = _Register->read(KeyRegOne);
	updatePC(rs);
}

void Mips::jal(int32 C){

	int32 PC = _Register->read($pc);
	_Register->update($ra, PC+8);

	int Address;
	Address = PC & 0xF0000000;
	C <<= 2;
	Address = Address | C;

	updatePC(Address*2);
}

void Mips::syscall(int32 Funct){

	switch(Funct){
		case 1:
			int Value;
			Value = _Register->read($a0);
			printf("%d\n", Value);
			break;
			

		case 4:

			break;

		case 5:
			int Result;
			
			scanf("%d", &Result);
			_Register->update($v0, Result);
			break;

		case 8:
		
			break;

		case 10:			
			_Register->update($FL, 1);

			break;

		case 12:
			char c;

			scanf("%c", &c);
			_Register->update($v0, c);

			break;

		case 34:
			
			Value = _Register->read($a0);
			printf("%08x\n", Value);
			break;

		case 36:
			unsigned int uValue;

			uValue = (unsigned int) _Register->read($a0);
			printf("%u\n",uValue);
			break;
	}

}