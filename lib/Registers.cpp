#include <stdlib.h>
#include <stdio.h>
#include "include/Registers.h"
#include "include/Defines.h"


Register::Register(){
	_Register = (int *) malloc(sizeof(int) * 36);

	// Inicia os valores padroes nos registradores
	Init();
}

void Register::Init(){


	// Inicia o registrador $ZERO
	update($zero, 0x00000000);
	// Inicia o registrador $GP
	update($gp, 0x00001800);
	// Inicia do registrador $SP
	update($sp, 0x00003ffc);
}

// Funcao para a atualizacao do registrador
void Register::update(int Key, int Value){
	if (Key == $sp ){
		if (Value < 0x3000){
			printf("Error: stack overflow.\n");
			_Register[$FL] = 1;
		}
	}
	_Register[Key] = Value;
}

// Funcao para leitura do registrador
int Register::read(int Key){
	return _Register[Key];
}

// Fucao para imprimir os valores dos registradores na tela
void Register::ShowRegisters(){
	for (int i = 0; i < 31; ++i){
		printf("$%d\t0x%08x\n", i, _Register[i]);
	}
	// Como eu armazeno o valor de pc como variando de 8 em 8
	// imprimo o valor de RA como sendo a metado do valor de configuração do emulador
	printf("$%d\t0x%08x\n", 31, _Register[31]/2);
}

// Funcao para desalocar a memoria alocada
void Register::FreeRegister(){
	free(_Register);
}