#include "include/Tools.h"

Tools::Tools(){}

int Tools::OpcodeDecode(int Bytes){
	// Shift no valor para retornar apenas as duas casas mais significativas
	Bytes = 0xFF000000 & Bytes;
	Bytes >>= 24;
	Bytes = 0xFF & Bytes;
	Bytes >>= 2;

	return Bytes;
}

// Verifica se o char passado é um numero de 0 a 9
int is_Number(char letter){
	return (letter > 47 && letter < 58); 
}

// Retorna o valor do hexadecimal em decimal
int HexaNumber(char letter){
	int Number = 0;

	if (is_Number(letter)){
		Number = letter - 48;
	}else{
		Number = letter - 87;
	}

	return Number;
}

int Tools::HexaDecode(char * Bytes){
	unsigned int value = 0;
	int base = 1;
	int i, j;

	// percorre a palavra de 32 bits convertende o valor 
	// de hexadecimal que esta em char para inteiro
	for (i = 7, j = 0; Bytes[j] != 0; i--, j++){
		value += HexaNumber(Bytes[i]) * base;
		// Vou multiplicando o valor da base por 16
		base <<= 4;
	}

	return value;
}

char * Tools::HexaEncode(int Bytes){

	char * Return = (char *) malloc(sizeof(char) * 8);

	sprintf(Return, "%08x", Bytes);

	return Return;

}

int Tools::RegisterRS(int Bytes){
	Bytes <<= 2;
	Bytes = 0x0FFFFFFF& Bytes;
	Bytes >>= 23;
	return Bytes;
}

int Tools::RegisterRT(int Bytes){
	Bytes >>= 1;
	Bytes = 0xFFFFF & Bytes;
	Bytes >>= 15;
	return Bytes;
}

int Tools::RegisterRD(int Bytes){
	Bytes = 0xFFFF & Bytes;
	Bytes >>= 11;
	return Bytes;
}

int Tools::RegisterImmediate(int Bytes){
	Bytes = 0x0000FFFF & Bytes;
	return Bytes;
}

int Tools::RegisterImmediateTypeJ(int Bytes){
	Bytes <<= 2;
	Bytes = 0x0FFFFFFF & Bytes;
	Bytes >>= 2;
	return Bytes;
}

int Tools::RegisterShamt(int Bytes){
	Bytes <<= 1;
	Bytes = 0x00000FFF & Bytes;
	Bytes >>= 7;
	return Bytes;
}

int Tools::RegisterFunct(int Bytes){
	Bytes <<= 2;
	Bytes = 0xFF & Bytes;
	Bytes >>= 2;
	return Bytes;
}