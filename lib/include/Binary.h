#ifndef __BINARY__

#define __BINARY__

#include "Defines.h"
#include <stdio.h>
#include <stdlib.h>

/*
	Classe responsável pela manipulação dos arquivos binarios
*/

class Binary{

	protected:
		// Variavel responsvel pela abertura do arquivo
		FILE * File;
		// Variavel para o retorno do numero de elementos lidos
		int numberOfData;
		int size;
		char * Path;

	public:
		// Variavel para o acesso dos dados
		int * _Binary;

		// Construtor da classe
		Binary();

		void setPath(char * Path, int size);
		void Init();
		// Função responsável por retorna o numero de elementos lidos
		int getNumberOfData();
		// Função para o desalocamento do array alocado
		void close();
	
};

#endif