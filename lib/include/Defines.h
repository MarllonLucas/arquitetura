#ifndef __DEFINES__

#define __DEFINES__

/**
	Enderecos dos registradores por nome
*/

#define int32 	int
#define int16 	short int

#define $zero 	0
#define $at 	1
#define $v0 	2
#define $v1 	3
#define $a0 	4
#define $a1 	5
#define $a2 	6
#define $a3 	7
#define $t0 	8
#define $t1 	9
#define $t2 	10
#define $t3 	11
#define $t4 	12
#define $t5 	13
#define $t6 	14
#define $t7 	15
#define $s0 	16
#define $s1 	17
#define $s2 	18
#define $s3 	19
#define $s4 	20
#define $s5 	21
#define $s6 	22
#define $s7 	23
#define $t8 	24
#define $t9 	25
#define $k0 	26
#define $k1 	27
#define $gp 	28
#define $sp 	29
#define $fp 	30
#define $ra 	31
#define $pc		32
#define HI 		33
#define LO 		34
// Registrador de controle de saida do programa
#define $FL		35

// char binario[16][4] = {"0000","0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"}

#define SIZE_MEMORY 4096 * 4 * 2
#define SIZE_TEXT 	4096 * 0x0fff

// Definições de Função Assembly

// Aritimetico
#define ADD 	0x00 //
#define ADDFNC	0x20

#define ADDI	0x08 //

#define ADDIU	0x09 //

#define ADDU 	0x00 //
#define ADDUFNC	0x21


#define SUB	 	0x00 //
#define SUBFNC	0x22

#define DIV 	0x00 //
#define DIVFNC 	0x1A

#define MULT 	0x00 //
#define MULTFNC	0x18

// Logico
#define AND 	0x00 //
#define ANDFNC 	0x24

#define ANDI 	0x0C //

#define OR	 	0x00 //
#define ORFNC 	0x25

#define ORI	 	0x0D //

// Shifter
#define SLL	 	0x00 //
#define SLLFNC 	0x00

#define SRA	 	0x00 //
#define SRAFNC 	0x03

#define SRL	 	0x00 //
#define SRLFNC	0x02

// Comparadores
#define SLT	 	0x00 //
#define SLTFNC 	0x2A

#define SLTI 	0x0A //

// Branch
#define BEQ 	0x04 //
#define BNE 	0x05 //

// Jump
#define J	 	0x02 //

#define JR	 	0x00
#define JRFNC 	0x08

#define JAL 	0x03

// Load
#define LW	 	0x23 //
#define LH	 	0x21 //
#define LB	 	0x20 //

// Store
#define SW	 	0x2B //
#define SH	 	0x29 //
#define SB	 	0x28 //

// Moves
#define MFHI 	0x00 //
#define MFHIFNC	0x10 

#define MFLO 	0x00 //
#define MFLOFNC	0x12

// Syscall
#define SYSCALL		0x00
#define SYSCALLFNC	0x0C

#define LUI		0x0F //

#endif