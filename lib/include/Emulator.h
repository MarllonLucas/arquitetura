#ifndef __EMULATOR__

#define __EMULATOR__

#include <stdio.h>
#include <stdlib.h>
#include "Memory.h"
#include "Registers.h"
#include "Mips.h"
#include "Tools.h"
#include "Defines.h"
#include "Binary.h"


class Emulator{
	protected:
		Register _Register = Register();
		Memory _RAM = Memory();
		Binary _BinaryMipsText = Binary();
		Binary _BinaryMipsData = Binary();
		Tools _Tools = Tools();
		Mips _MIPS = Mips(&_Register, &_RAM);

	public:
		Emulator(char * PathText,char * PathData);

		void run();
		void ShowResults();
		void close();
		void CopyTextToMemory();
		void CopyDataToMemory();
	
};


#endif