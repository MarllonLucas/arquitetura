#ifndef __MEMORY__

#define __MEMORY__

#include <stdlib.h>
#include "Tools.h"

class Memory{
	protected:
		Tools _Tools = Tools();

	public:
		char * _RAM;
		Memory();
		void FreeMemory();

		void ShowMemory();
		void update(int Adreess, int Value);
		int read(int Adreess);
		void Init();
		void Clean();
		void Show();

};

#endif