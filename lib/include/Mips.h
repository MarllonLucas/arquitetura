#ifndef __MIPS__

#define __MIPS__

#include <stdlib.h>
#include <stdlib.h>
#include <stdio.h>

#include "Registers.h"
#include "Memory.h"
#include "Defines.h"
#include "Tools.h"

class Mips{
	
	protected:
		Register * _Register;
		Memory * _RAM;
		Tools _Tools = Tools();

	public:
		// Construtor da classe
		Mips(Register * REG, Memory * MEM);

		// Funcoes de Aritmetica

		// Soma de registradores
		void add 	(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		void addu 	(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		// Soma de registradores com imediato
		void addi 	(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate);
		// Soma de registradores com imediato sem sinal
		void addiu	(int32 KeyRegOne, int32 KeyRegTwo, unsigned int32 Imdiate);
		// Subtracao de registradores
		void sub	(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		// Multiplicacao de registradores
		void mult	(int32 KeyRegOne, int32 KeyRegTwo);
		// Divisao de registradores
		void div	(int32 KeyRegOne, int32 KeyRegTwo);

		// Funcoes de Tranferencia de dados
		
		// Leitura de palavra de 4 bytes
		void lw(int32 KeyRegOne, int32 Adreess);
		// Leitura de palavra de 2 bytes
		void lh(int32 KeyRegOne, int32 Adreess);
		// Leitura de palavra de 1 bytes
		void lb(int32 KeyRegOne, int32 Adreess);
		
		// Escrita de palavra de 4 bytes
		void sw(int32 KeyRegOne, int32 Adreess);
		// Escrita de palavra de 2 bytes
		void sh(int32 KeyRegOne, int32 Adreess);
		// Escrita de palavra de 1 bytes
		void sb(int32 KeyRegOne, int32 Adreess);

		// Load Upper Immediate
		void lui(int32 KeyRegOne, int32 C);
		
		void mfhi(int32 KeyRegister);
		void mflo(int32 KeyRegister);

		// Logica

		// 'E' logico entre registradores
		void And(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		// 'E' logico entre registrador e imediato
		void Andi(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate);
		// 'Ou' logico entre registradores
		void Or(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		// 'Ou' logico entre registrador e imediato
		void Ori(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate);

		// Verifica se KeyRegTwo eh menor que KeyRegThree
		void slt(int32 KeyRegOne, int32 KeyRegTwo, int32 KeyRegThree);
		// Verifica se KeyRegTwo eh menor que o imediato
		void slti(int32 KeyRegOne, int32 KeyRegTwo, int32 Immediate);

		// Deslocamento de bits
		
		// Deslocamento de bits para a esquerda
		void sll(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt);
		// Deslocamento de bits para a direita
		void srl(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt);
		// Deslocamento de bits para a direita com extensao de sinal
		void sra(int32 KeyRegOne, int32 KeyRegTwo, int32 Shamt);

		// Desvio condicional

		// Se KeyRegOne == KeyRegTwo entao PC = PC + 4 + (C << 2)
		int32 beq(int32 KeyRegOne, int32 KeyRegTwo);
		// Se KeyRegOne != KeyRegTwo entao PC = PC + 4 + (C << 2)
		int32 bne(int32 KeyRegOne, int32 KeyRegTwo);

		// Desvio incondicional
		// Jump PC = (PC[31-28] + 4) || (C << 2)
		void j(int32 C);
		// Jump Registrer PC = KeyRegOne
		void jr(int32 KeyRegOne);
		// Jump and Link RA = PC + 4; PC = (PC[31 − 28] + 4)||(C << 2)
		void jal(int32 C);

		void syscall(int32 Funct);

		void updatePC(int32 PC = 0);

};



#endif