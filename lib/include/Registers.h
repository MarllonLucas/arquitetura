#ifndef __REGISTER__

#define __REGISTER__

#include <stdlib.h>

class Register{
	
	protected:
		int * _Register;

	public:
		Register();
		// Inicia os valores padroes dos registradores
		void Init();
		// Funcao responsavel pela contrucao da memoria dos registradores
		void ShowRegisters();
		// Desaloca memoria alocada
		void FreeRegister();

		// Funcao de atualizacao dos registradores
		void update(int Key, int Value);

		// Funcao de retorno de valor do registrador
		int read(int Key);

};



#endif