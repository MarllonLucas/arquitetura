#ifndef __TOOLS__

#define __TOOLS__

#include <stdio.h>
#include "Defines.h"
#include <stdlib.h>

class Tools{
	public:
		Tools();
		int OpcodeDecode(int Bytes);
		int HexaDecode(char * Bytes);
		char * HexaEncode(int Bytes);
		int RegisterRS(int Bytes);
		int RegisterRT(int Bytes);
		int RegisterRD(int Bytes);
		int RegisterImmediate(int Bytes);
		int RegisterImmediateTypeJ(int Bytes);
		int RegisterShamt(int Bytes);
		int RegisterFunct(int Bytes);
};

#endif