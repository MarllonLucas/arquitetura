#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lib/include/Mips.h"
#include "lib/include/Registers.h"
#include "lib/include/Memory.h"
#include "lib/include/Emulator.h"
#include "lib/include/Binary.h"
#include "lib/include/Tools.h"
#include "lib/include/Defines.h"

using namespace std;

int main(int argc, char const *argv[]){
	
	char * SectionText = (char *) malloc(sizeof(char) * 128);
	char * SectionData = (char *) malloc(sizeof(char) * 128);


	strcpy(SectionText, argv[1]);
	strcpy(SectionData, argv[2]);

	Emulator Emulador = Emulator(SectionText, SectionData);

	// Inicia a emulação
	Emulador.run();

	// Mostra a memoria gerada
	Emulador.ShowResults();

	// Libera a memória alocada
	Emulador.close();

	free(SectionText);
	free(SectionData);

	return 0;
}