### Makefile ###

all: arq clean

arq: main.o register.o memory.o mips.o emulator.o binary.o tools.o
	g++ -std=c++11 -g -o main main.o mips.o register.o memory.o emulator.o binary.o tools.o

main.o: main.cpp lib/include/Defines.h
	g++ -std=c++11 -Wall -pedantic -c -g -o main.o main.cpp

mips.o: lib/Mips.cpp lib/include/Mips.h
	g++ -std=c++11 -Wall -pedantic -c -g -o mips.o lib/Mips.cpp

register.o: lib/Registers.cpp lib/include/Registers.h
	g++ -std=c++11 -Wall -pedantic -c -g -o register.o lib/Registers.cpp

memory.o: lib/Memory.cpp lib/include/Memory.h 
	g++ -std=c++11 -Wall -pedantic -c -g -o memory.o lib/Memory.cpp

emulator.o: lib/Emulator.cpp lib/include/Emulator.h
	g++ -std=c++11 -Wall -pedantic -c -g -o emulator.o lib/Emulator.cpp

binary.o: lib/Binary.cpp lib/include/Binary.h
	g++ -std=c++11 -Wall -pedantic -c -g -o binary.o lib/Binary.cpp

tools.o: lib/Tools.cpp lib/include/Tools.h
	g++ -std=c++11 -Wall -pedantic -c -g -o tools.o lib/Tools.cpp

clean:
	rm *.o

